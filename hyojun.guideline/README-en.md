# Hyojun.Guideline
`v0.1.1`

This is the Guideline Bootstrap used in Hyojun projects. Has the base templates and a simple page generator using grunt and mustache for standalone projects.

Check a guideline example to check how elements and classes work at [http://fbiz.bitbucket.org/hyojun.guideline/](http://fbiz.bitbucket.org/hyojun.guideline/)

Page content:

[TOC]

[↩][top]

## Installation

You'll need to have [nodejs][] — for grunt, bower / npm packages — and [ruby][]  — for SASS — on your computer in order to use Hyojun.Guideline.

1. Install [grunt-cli][]:

        sudo npm install grunt-cli -g

2. Install [bower][]:

        sudo npm install bower -g

### Installing it in your project

For now the instalation in another project is manual. To achieve it, create a `bower.json` and download the necessary files:

1. Create `bower.json`:

        bower init

2. Download Hyojun.guideline as a bower dependency:

        bower install https://bitbucket.org/fbiz/hyojun.guideline.git --save-dev

There's 2 ways of using the guideline: mustache to generate the static pages or to migrate it (manually for now) to another environment, such as PHP, ASP.NET, etc:

### Mustache only

For static pages such as prototypes or documentations, it's possible to use grunt + mustache to generate the website. First create and download all nodejs dependencies to use with Grunt:

1. Get `package.json` from Hyojun.guideline:

        cp bower_components/Hyojun.Guideline/package.json .

2. Download necessary packages:

        npm install

    _(Not elegant, but soon there'll be an installer here)_

3. Then, copy the necessary files from bower components (will be automated):

        cp -r bower_components/Hyojun.Guideline/{generator,Gruntfile.js,.jshintrc} .
        echo "{}" > generator/grunt/js.json
        git clone https://bitbucket.org/fbiz/hyojun.sass-standards.git
        mkdir assets
        mv hyojun.sass-standards/sass assets/sass
        rm -rf hyojun.sass-standards

Create `assets/sass/output/1306-desktop/guideline.scss` and place the imports:

    #!sass
    @import
        // guideline lib
        "Hyojun.Guideline/assets/sass/source/common/wrappers/lib",

        // Project's specific core files comes here, to
        // customize guideline colors and gridsystem page widths.

        // guideline elements (desktop, large screen, version)
        "Hyojun.Guideline/assets/sass/output/320-mobile/guideline.scss"; // desktop version

Create `assets/sass/output/320-mobile/guideline.scss` and place the imports:

    #!sass
    @import
        // guideline lib
        "Hyojun.Guideline/assets/sass/source/common/wrappers/lib",

        // Project's specific core files comes here, to
        // customize guideline colors and gridsystem page widths.

        // guideline elements (mobile, smaller screen, version)
        "Hyojun.Guideline/assets/sass/output/1306-desktop/guideline.scss"; // mobile version

Inside `generator/guideline/data/gl-base.json` file, point `guideline.js` to bower folder:

    ...
    "js": {
        "core": "/bower_components/Hyojun.Guideline/assets/js/dist/guideline.js"
    },
    ...

Run grunt `render` task to build HTML and SASS files:

    grunt render

Run a local server to see the page running, eg.:

    python -m SimpleHTTPServer

Finally, access the website in http://localhost:8000/guideline/

### ASP.NET

There's an ASP.NET template applied in [Hyojun.Bootstrap][]. To use it just download and install from the project page.

### Contributing with other Languages

Read this document to understand how you can help with this project (TODO).

[↩][top]

## Changelog

Version history is available at [CHANGELOG.md][].

[top]: #markdown-header-template

[ruby]: https://www.ruby-lang.org/en/
[bower]: http://bower.io/
[nodejs]: http://nodejs.org/download/
[grunt-cli]: http://gruntjs.com/getting-started/
[Hyojun.Bootstrap]: https://bitbucket.org/fbiz/tecnologia_modulo-padrao
[CHANGELOG.md]: https://bitbucket.org/fbiz/hyojun.guideline/src/HEAD/CHANGELOG.md