// @grunt -task=comp-js -page=guideline

define(function (require) {
	require('requirejs');
	require('jquery');
	require('highlightjs');
	require('colorjs');
	require('modernizr');
});

require([
	"mod/colors/sass-parser",
	"mod/colors/color-generator",
	"mod/colors/color-palette",
	"mod/navigation/toggle-navigation",
	"mod/navigation/permalink",
	"mod/ui/toggle-transparency"
], function (
	sassParser,
	colorGenerator,
	colorPalette,
	toggleNavigation,
	permalink,
	toggleTransparency
) {
	$(function(){
		var sass, palette, generator;

		// Color palette stuff
		palette = colorPalette.plugin();
		colorPalette.status('loading');

		generator = colorGenerator.plugin();
		colorGenerator.status('loading');

		if (generator.length > 0 || palette.length > 0) {
			sass = sassParser.plugin()[0];
			if (sass) {
				sass.evt.bind('load-success', function () {
					colorGenerator.status('color-ready', {
						mainColors: sass.getMainColors(),
						allColors: sass.colors
					});
					colorPalette.status('color-ready', {
						allColors: sass.colors
					});
				});
				sass.init();
			}
		}

		// Code highlighting
		hljs.initHighlighting();

		// Mobile - Toggle Navigation
		toggleNavigation.plugin();

		// Generate permalink links on titles
		permalink.plugin();

		// Generate permalink links on titles
		toggleTransparency.plugin();
	});
} );