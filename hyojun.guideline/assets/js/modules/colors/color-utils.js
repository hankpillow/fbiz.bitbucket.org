// @grunt -task=comp-js -page=guideline

// needs net.brehaut.Color
define([], function(){
	"use strict";

	var colorJs = net.brehaut.Color,
		_calculateHSLDiff = function (p_hsl1, p_hsl2, p_action) {
		var result = [];
		if ((!p_hsl1 || !p_hsl2) || (p_hsl1.length !== p_hsl2.length)) {
			return null;
		}

		$.each(p_hsl1, function (i, item) {
			if (p_action === 'encode') {
				result[i] = Math.round((p_hsl2[i] - p_hsl1[i]) * 100) / 100;
			} else {
				result[i] = Math.round((p_hsl1[i] + p_hsl2[i]) * 100) / 100;
			}
		});

		return result;
	};

	return {
		encodeHSLDiff: function (hsl1, hsl2) {
			var result = _calculateHSLDiff(hsl1, hsl2, 'encode');
			return colorJs({
				hue: result[0],
				saturation: result[1],
				lightness: result[2]
			});
		},
		decodeHSLDiff: function (hsl1, hsl2) {
			var result = _calculateHSLDiff(hsl1, hsl2, 'decode');
			return colorJs({
				hue: result[0],
				saturation: result[1],
				lightness: result[2]
			});
		},
		getEncodeHSLDiffValues: function (hsl1, hsl2) {
			return _calculateHSLDiff(hsl1, hsl2, 'encode');
		},
		getDecodeHSLDiffValues: function (hsl1, hsl2) {
			return _calculateHSLDiff(hsl1, hsl2, 'decode');
		}
	};
});