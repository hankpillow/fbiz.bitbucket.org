# F.biz Bitbucket user website

Este projeto contém apenas as páginas de exemplos ou documentações dos repositórios originais baixadas utilizando `git read-tree`, para facilitar a atualização.

Estas cópias tem um pequeno nível de alteração, como inserção do ribbon "fork on bitbucket", customização dos caminhos para funcionar no ambiente ou em casos de sites gerados, os arquivos finais (que normalmente não existem no repositório original). Portanto, apesar de raro, conflitos podem ocorrer ao fazer o subtree merge.

Abaixo a lista de projetos e como atualizá-los:

## Hyojun.git-immersion

Projeto é um subtree de http://bitbucket.org/fbiz/hyojun.git-immersion.git. Para atualizá-lo, apenas digite:

    git pull -s subtree git@bitbucket.org:fbiz/hyojun.git-immersion.git master

## Hyojun.guideline

Projeto é um subtree de http://bitbucket.org/fbiz/hyojun.guideline.git. Para atualizá-lo, apenas digite:

    git pull -s subtree git@bitbucket.org:fbiz/hyojun.guideline.git master